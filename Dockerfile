FROM registry.gitlab.com/iitp/base-containers/alpine:latest
LABEL maintainer="Sean Erswell-Liljefelt"
USER root
RUN apk add --no-cache nginx && \
    mkdir -p /www /etc/nginx/sites-available /etc/nginx/sites-enabled /etc/nginx/ssl /var/tmp/nginx /var/lib/nginx/logs && \
    touch /var/lib/nginx/logs/error.log
COPY conf/nginx.conf /etc/nginx
COPY conf/default /etc/nginx/sites-available
COPY certs /etc/nginx/ssl
COPY content /www
RUN ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default && \
    chown -R app:app /var/lib/nginx /www /etc/nginx /var/tmp/nginx /var/log/nginx && \
    rm -f /etc/nginx/conf.d/*
USER app
EXPOSE 8080 8443
CMD /usr/sbin/nginx -g "daemon off;"
