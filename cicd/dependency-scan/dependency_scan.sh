#!/bin/sh
DEPCHECK_VERSION=5.0.0
echo "PROJECT: $CI_PROJECT_NAME"
echo "BRANCH: $CI_COMMIT_REF_NAME"
echo "COMMIT: $CI_COMMIT_SHORT_SHA"
echo "SCAN DIRECTORY: $CI_PROJECT_DIR"
echo "OWASP DEPENDENCY CHECKER VERSION: $DEPCHECK_VERSION"
echo "******** BEGINNING INSTALL & UPDATE ********"
# Pre-reqs
apk add curl openjdk8-jre gnupg && \
curl -sSL https://bintray.com/user/downloadSubjectPublicKey?username=jeremy-long | gpg --import - && \
# Install
wget -O /tmp/dep-check.zip https://dl.bintray.com/jeremy-long/owasp/dependency-check-$DEPCHECK_VERSION-release.zip && \
unzip -d /tmp/ /tmp/dep-check.zip && \
mkdir -p "$CI_PROJECT_DIR"/depcheck-reports && \
# Execute
echo "******** BEGINNING DEPENDENCY SCAN ********"
/tmp/dependency-check/bin/dependency-check.sh --project "$CI_PROJECT_NAME" --scan "$CI_PROJECT_DIR" \
    --out "$CI_PROJECT_DIR"/depcheck-reports \
    --format "ALL" \
    --failOnCVSS "4" \
    --enableExperimental
