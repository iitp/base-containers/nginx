#!/bin/sh
# Determine image path tag from branch/tag
export CI_APPLICATION_REPOSITORY=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG/$CI_PROJECT_NAME
if [ -z "$CI_COMMIT_TAG" ]; then
    echo "Build request coming from feature or master branch."
    export CI_APPLICATION_TAG=$CI_COMMIT_SHORT_SHA
else
    echo "Build request coming from a tag in pre-production."
    export CI_APPLICATION_TAG=$CI_COMMIT_TAG
fi

docker run -d --name db arminc/clair-db:latest
docker run -p 6060:6060 --link db:postgres -d --name clair --restart on-failure arminc/clair-local-scan:"$CLAIR_LOCAL_SCAN_VERSION"
apk add -U wget ca-certificates
docker pull "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"
wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64
mv clair-scanner_linux_amd64 clair-scanner
chmod +x clair-scanner
touch clair-whitelist.yml
retries=0
echo "Waiting for clair daemon to start"
while( ! wget -T 10 -q -O /dev/null http://127.0.0.1:6060/v1/namespaces ) ; do sleep 1 ; echo -n "." ; if [ $retries -eq 10 ] ; then echo " Timeout, aborting." ; exit 1 ; fi ; retries=$(("$retries"+1)) ; done
./clair-scanner -c http://127.0.0.1:6060 --ip "$(hostname -i)" -r image-scanning-report.json -l clair.log -w clair-whitelist.yml "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG" || true
