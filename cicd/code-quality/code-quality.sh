#!/bin/sh
export CQ_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')

docker run --env SOURCE_CODE="$PWD" \
            --volume "$PWD":/code \
            --volume /var/run/docker.sock:/var/run/docker.sock \
            "registry.gitlab.com/gitlab-org/security-products/codequality:$CQ_VERSION" /code