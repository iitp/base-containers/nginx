#!/bin/sh
TEST_COUNT=0
PASS_COUNT=0

echo "############ UNIT TEST RESULTS ############" | tee unit_test_results.txt
echo ""
echo "INFO   : Container running as user : $(whoami)"  | tee -a unit_test_results.txt
echo ""
echo "CI REFERENCES"
echo "-------------"
echo "EXECUTED AT : $(date '+%Y-%m-%d %H:%M:%S')" | tee -a unit_test_results.txt
echo "COMMIT : $CI_COMMIT_SHA" | tee -a unit_test_results.txt
echo "REF : $CI_COMMIT_REF_NAME" | tee -a unit_test_results.txt
echo "PIPELINE : $CI_PIPELINE_URL" | tee -a unit_test_results.txt
echo "JOB : $CI_JOB_URL" | tee -a unit_test_results.txt
echo ""

# Verify the entrypoint
TEST_COUNT=$((TEST_COUNT+1))
if /usr/sbin/nginx; then
    PASS_COUNT=$((PASS_COUNT+1))
    echo "TEST $TEST_COUNT : Nginx started : PASS" | tee -a unit_test_results.txt
else
    echo "TEST $TEST_COUNT : Nginx not started : FAIL" | tee -a unit_test_results.txt
fi

# Verify non-root context
TEST_COUNT=$((TEST_COUNT+1))
if [ "$(id -u)" != 0 ] ; then
    PASS_COUNT=$((PASS_COUNT+1))
    echo "TEST $TEST_COUNT : User context is non-root : PASS" | tee -a unit_test_results.txt
else
    echo "TEST $TEST_COUNT : User context is non-root : FAIL" | tee -a unit_test_results.txt
fi

# Verify package installation
TEST_COUNT=$((TEST_COUNT+1))
if nginx -v 2>/dev/null ; then
    PASS_COUNT=$((PASS_COUNT+1))
    echo "TEST $TEST_COUNT : Package nginx installed : PASS" | tee -a unit_test_results.txt
else
    echo "TEST $TEST_COUNT : Package nginx installed : FAIL" | tee -a unit_test_results.txt
fi

# Verify HTTP works
TEST_COUNT=$((TEST_COUNT+1))
if wget -T 10 -q -O /dev/null http://127.0.0.1:8080 ; then
    PASS_COUNT=$((PASS_COUNT+1))
    echo "TEST $TEST_COUNT : HTTP protocol functioning : PASS" | tee -a unit_test_results.txt
else
    echo "TEST $TEST_COUNT : HTTP protocol functioning : FAIL" | tee -a unit_test_results.txt
fi

# Verify HTTPS works
TEST_COUNT=$((TEST_COUNT+1))
if wget -T 10 -q -O /dev/null https://127.0.0.1:8443 --no-check-certificate; then
    PASS_COUNT=$((PASS_COUNT+1))
    echo "TEST $TEST_COUNT : HTTPS protocol functioning : PASS" | tee -a unit_test_results.txt
else
    echo "TEST $TEST_COUNT : HTTPS protocol functioning : FAIL" | tee -a unit_test_results.txt
fi

# Error pages reachable
TEST_COUNT=$((TEST_COUNT+1))
if wget -T 10 -q -O /dev/null http://127.0.0.1:8080/error/404.html ; then
    PASS_COUNT=$((PASS_COUNT+1))
    echo "TEST $TEST_COUNT : Error pages reachable : PASS" | tee -a unit_test_results.txt
else
    echo "TEST $TEST_COUNT : Error pages reachable : FAIL" | tee -a unit_test_results.txt
fi

# Need to stop nginx so that the job realises it is finished
killall -9 nginx

################ SUMMARY ################
echo "$PASS_COUNT tests out of $TEST_COUNT succeeded..."
if  [ "$PASS_COUNT" -lt "$TEST_COUNT" ] ; then
    echo "SUMMARY : FAIL" && exit 1
else
    echo "SUMMARY : PASS" && exit 0
fi
