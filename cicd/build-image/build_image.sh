#!/bin/sh
# Informational output for debug and historical reference
echo "CI_COMMIT_TAG=$CI_COMMIT_TAG"
echo "CI_COMMIT_REF_SLUG=$CI_COMMIT_REF_SLUG"
echo "CI_COMMIT_SHORT_SHA=$CI_COMMIT_SHORT_SHA"
echo "CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE"
# Determine image path tag from branch/tag
export CI_APPLICATION_REPOSITORY=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG/$CI_PROJECT_NAME
if [ -z "$CI_COMMIT_TAG" ]; then
    echo "Build request coming from feature or master branch."
    export CI_APPLICATION_TAG=$CI_COMMIT_SHORT_SHA
else
    echo "Build request coming from a tag in pre-production."
    export CI_APPLICATION_TAG=$CI_COMMIT_TAG
fi
echo "CI_APPLICATION_REPOSITORY=$CI_APPLICATION_REPOSITORY"
echo "CI_APPLICATION_TAG=$CI_APPLICATION_TAG"
echo "Building image : $CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"

# Adding commit and branch to non-prod environments to footer for demo purposes
if [ "$CI_COMMIT_REF_NAME" != "production" ] ; then
    sed -i "s%</HTML>%<footer>BRANCH: ${CI_COMMIT_REF_NAME} COMMIT: ${CI_COMMIT_SHORT_SHA}</footer></HTML>%" content/index.html
fi

docker build --rm --no-cache --tag "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG" .
docker push "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"
