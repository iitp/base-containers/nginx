#!/bin/sh
export SAST_VERSION=${SP_VERSION:-$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')}
# this is required to avoid undesirable reset of Docker image ENV variables being set on build stage
function propagate_env_vars() {
    CURRENT_ENV=$(printenv)
    for VAR_NAME; do
        echo $CURRENT_ENV | grep "${VAR_NAME}=" > /dev/null && echo "--env $VAR_NAME "
    done
}
docker run \
    $(propagate_env_vars \
    SAST_ANALYZER_IMAGES \
    SAST_ANALYZER_IMAGE_PREFIX \
    SAST_ANALYZER_IMAGE_TAG \
    SAST_DEFAULT_ANALYZERS \
    SAST_EXCLUDED_PATHS \
    SAST_BANDIT_EXCLUDED_PATHS \
    SAST_BRAKEMAN_LEVEL \
    SAST_GOSEC_LEVEL \
    SAST_FLAWFINDER_LEVEL \
    SAST_GITLEAKS_ENTROPY_LEVEL \
    SAST_DOCKER_CLIENT_NEGOTIATION_TIMEOUT \
    SAST_PULL_ANALYZER_IMAGE_TIMEOUT \
    SAST_RUN_ANALYZER_TIMEOUT \
    ) \
    --volume "$PWD:/code" \
    --volume /var/run/docker.sock:/var/run/docker.sock \
    "registry.gitlab.com/gitlab-org/security-products/sast:$SAST_VERSION" /app/bin/run /code

TOTAL_VULN=$(cat gl-sast-report.json | grep severity | wc -l)
TOTAL_CRIT=$(cat gl-sast-report.json | grep "\"severity\"\: \"Critical\"" | wc -l)
TOTAL_HIGH=$(cat gl-sast-report.json | grep "\"severity\"\: \"High\"" | wc -l)
TOTAL_MEDIUM=$(cat gl-sast-report.json | grep "\"severity\"\: \"Medium\"" | wc -l)
TOTAL_LOW=$(cat gl-sast-report.json | grep "\"severity\"\: \"Low\"" | wc -l)
TOTAL_INFO=$(cat gl-sast-report.json | grep "\"severity\"\: \"Info\"" | wc -l)
TOTAL_UNDEF=$(cat gl-sast-report.json | grep "\"severity\"\: \"Undefined\"" | wc -l)
TOTAL_UNKNOWN=$(cat gl-sast-report.json | grep "\"severity\"\: \"Unkown\"" | wc -l)

echo "****** SUMMARY ******"
echo ""
echo "TOTAL VULNERABILITIES FOUND : $TOTAL_VULN"
echo "TOTAL CRITICAL : $TOTAL_CRIT"
echo "TOTAL HIGH : $TOTAL_HIGH"
echo "TOTAL MEDIUM : $TOTAL_MEDIUM"
echo "TOTAL LOW : $TOTAL_LOW"
echo "TOTAL INFO : $TOTAL_INFO"
echo "TOTAL UNDEFINED : $TOTAL_UNDEF"
echo "TOTAL UNKNOWN : $TOTAL_UNKNOWN"

if [ "$TOTAL_VULN" -gt "0" ] ; then
    exit 1
else
    exit 0
fi