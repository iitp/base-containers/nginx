import { check, group, sleep } from "k6";
import http from "k6/http";

// Test configuration
export let options = {
    // Rampup for 15s from 1 to 25, stay at 25, and then down to 0
    stages: [
        { duration: "60s", target: 500 },
        { duration: "60s", target: 500 },
        { duration: "60s", target: 0 }
    ],
    thresholds: {
        "http_req_duration": ["p(95)<10"]
    },
    ext: {
        loadimpact: {
            name: "test.loadimpact.com"
        }
    }
};

// User scenario
export default function() {
    group("Front page", function() {
        // Make a request for the front page HTML (this will not fetch static resources referenced by HTML file)
        let res = http.get("http://pre-production.io11.com/");

        // Make sure the status code is 200 OK
        check(res, {
            "is status 200": (r) => r.status === 200
        });

        // Simulate user reading the page
        sleep(5);
    });
}
