#!/bin/sh
pwd
docker run -v $(pwd)/cicd/load-test:/tmp loadimpact/k6 run --out json=/tmp/load-test-results.json /tmp/main-test.js
