#!/bin/sh
# Determine image path tag from branch/tag
export CI_APPLICATION_REPOSITORY=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG/$CI_PROJECT_NAME
if [ -z "$CI_COMMIT_TAG" ]; then
    echo "Build request coming from feature or master branch."
    export CI_APPLICATION_TAG=$CI_COMMIT_SHORT_SHA
else
    echo "Build request coming from a tag in pre-production."
    export CI_APPLICATION_TAG=$CI_COMMIT_TAG
fi
echo "CI_APPLICATION_REPOSITORY=$CI_APPLICATION_REPOSITORY"
echo "CI_APPLICATION_TAG=$CI_APPLICATION_TAG"
echo "Tagging $CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG as latest"

docker pull "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"
docker tag "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG" "$CI_APPLICATION_REPOSITORY:latest"
docker push "$CI_APPLICATION_REPOSITORY:latest"