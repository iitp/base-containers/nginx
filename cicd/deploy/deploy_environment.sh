#!/bin/sh
# Determine image path tag from branch/tag
export CI_APPLICATION_REPOSITORY=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG/$CI_PROJECT_NAME
if [ -z "$CI_COMMIT_TAG" ]; then
    echo "Build request coming from feature or master branch."
    export CI_APPLICATION_TAG=$CI_COMMIT_SHORT_SHA
else
    echo "Build request coming from a tag in pre-production."
    export CI_APPLICATION_TAG=$CI_COMMIT_TAG
fi

echo "Using branch $CI_COMMIT_REF_SLUG"
echo "Image to deploy is $CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"
echo "Environment Name : $CI_ENVIRONMENT_NAME"
echo "Environment Slug : $CI_ENVIRONMENT_SLUG"
ENV_URL=$(echo "$CI_ENVIRONMENT_URL" | cut -d'/' -f3)
echo "Environment URL : $CI_ENVIRONMENT_URL"

# If private project, will need registry credentials
if [ "$CI_PROJECT_VISIBILITY" != "public" ]; then
    secret_name='gitlab-registry'
else
    secret_name=''
fi

# Modify deploy template
sed -i "s/__ENV_URL__/${ENV_URL}/" cicd/deploy/k8s-manifest.yaml
sed -i "s/__CI_ENVIRONMENT_SLUG__/${CI_ENVIRONMENT_SLUG}/" cicd/deploy/k8s-manifest.yaml
sed -i "s%__CI_APPLICATION_REPOSITORY__\\:__CI_APPLICATION_TAG__%${CI_APPLICATION_REPOSITORY}\\:${CI_APPLICATION_TAG}%" cicd/deploy/k8s-manifest.yaml
sed -i "s/__KUBE_NAMESPACE__/${KUBE_NAMESPACE}/" cicd/deploy/k8s-manifest.yaml
sed -i "s/__KUBE_INGRESS_BASE_DOMAIN__/${KUBE_INGRESS_BASE_DOMAIN}/" cicd/deploy/k8s-manifest.yaml

# Deploy
kubectl apply -f cicd/deploy/k8s-manifest.yaml
