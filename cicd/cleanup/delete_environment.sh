#!/bin/sh
kubectl delete -f cicd/deploy/k8s-manifest.yaml
kubectl delete secret --ignore-not-found -n "$KUBE_NAMESPACE" gitlab-registry
