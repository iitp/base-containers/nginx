#!/bin/sh
# Non-tagged builds (feature and master branches)
if [[ -z "$CI_COMMIT_TAG" ]]; then
    echo "Build request coming from branch or MR .."
    export CI_APPLICATION_REPOSITORY="$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG/$CI_PROJECT_NAME"
    export CI_APPLICATION_TAG="$CI_COMMIT_SHORT_SHA"
else
    echo "Build request coming from tag.."
    # Release Candidate Tag (preproduction)
    if echo $tag | grep -E '^[0-9]+\.[0-9]+\.[0-9]+\-rc[0-9]+$' ; then
        echo "Release Candidate Tagged $CI_COMMIT_TAG detected"
        export CI_APPLICATION_REPOSITORY="$CI_REGISTRY_IMAGE/preproduction/$CI_PROJECT_NAME"
        export CI_APPLICATION_TAG="$CI_COMMIT_TAG"
    # Release Tag (production)
    elif echo $tag | grep -E '^r[0-9]+\.[0-9]+\.[0-9]+$' ; then
        echo "Release Tagged $CI_COMMIT_TAG detected"
        export CI_APPLICATION_REPOSITORY="$CI_REGISTRY_IMAGE/$CI_PROJECT_NAME"
        export CI_APPLICATION_TAG="$CI_COMMIT_TAG"
    else
        echo "This was tagged incorrectly."
        echo 'For pre-production use "NUM.NUM.NUM-rcNUM"'
        echo 'For production use "rNUM.NUM.NUM"'
        exit 1
    fi
fi