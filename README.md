# iitp/nginx

# Status

Master: [![pipeline status](https://gitlab.com/iitp/base-containers/nginx/badges/master/pipeline.svg)](https://gitlab.com/iitp/base-containers/nginx/commits/master) Pre-Production: [![pipeline status](https://gitlab.com/iitp/base-containers/nginx/badges/preproduction/pipeline.svg)](https://gitlab.com/iitp/base-containers/nginx/commits/preproduction) Production: [![pipeline status](https://gitlab.com/iitp/base-containers/nginx/badges/production/pipeline.svg)](https://gitlab.com/iitp/base-containers/nginx/commits/production)

# Size and details
Size `~5mb`

# Purpose
Based on the hardened iitp/alpine:latest image - provides a simple yet hardened NGINX server for HTML content running as a non root user on http (port: 8080) & https (port: 8443)

# Highlights
* Nginx running as a non-root user (app)
* HTTP & HTTPS configured
* Self signed 10 year localhost certificate provided
* TLS v1.2 only
* HIGH ciphers with MD5 variations disabled
* X-Forwarded-For header enabled
* access to . files disabled
* Dropin config directory enabled at /etc/nginx/conf.d
* Server tokens disabled
* Max Body Size = 100M
* Due to non-root user ports 8080 and 8443 are utilised

# Layout & Special Information

As a simple HTML server there are probably three things you need to know.
* Content goes into the /www directory
* Additional configs can be added to /etc/nginx/conf.d
* Default site configuration is in /etc/nginx/sites-available/default.conf
